module full_adder  
(
    input logic a, 
    input logic b, 
    input logic cin,
    output logic cout, 
    output logic sout
);

assign sout = a ^ b ^ cin;
assign cout = ((a ^ b) & cin) | (a & b);

endmodule