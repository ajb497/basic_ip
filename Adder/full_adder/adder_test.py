import cocotb
from cocotb.types import Logic
from cocotb.triggers import Timer


    

async def run_inputs(dut, a, b, cin):
    dut.a.setimmediatevalue(a)
    dut.b.setimmediatevalue(b)
    dut.cin.setimmediatevalue(cin)
    await Timer(1, units="ns")
    assert dut.cout.value == a ^ b ^ cin
    assert dut.sout.value == ((a ^ b) & cin) | (a & b)


@cocotb.test()
async def my_first_test(dut):
    """Try accessing the design."""
    #                     a  b  cin
    await run_inputs(dut, 0, 0, 0)
    await run_inputs(dut, 1, 0, 0)
    await run_inputs(dut, 0, 1, 0)
    await run_inputs(dut, 1, 1, 0)
    await run_inputs(dut, 0, 0, 1)
    await run_inputs(dut, 1, 0, 1)
    await run_inputs(dut, 0, 1, 1)
    await run_inputs(dut, 1, 1, 1)
