module lcu64 
(
    input  logic        c0, 
    input  logic [63:0] a, 
    input  logic [63:0] b,
    output logic        c64,
    output logic [63:0] sout,
    output logic        pg, 
    output logic        gg
);

    //generate 4 full_adders

    //assign
    logic c_wires    [3:0];
    logic c_out      [3:0];
    logic prop_wires [3:0];
    logic gen_wires  [3:0];

    assign c_wires[0] = c0;
    

    genvar i;
    generate
        for (i = 0; i < 4; i++) begin

            lcu16 lcu(c_wires[i], a[(i*16)+15:(i*16)], b[(i*16)+15:(i*16)], c_out[i], sout[(i*16)+15:(i*16)], prop_wires[i], gen_wires[i]);

            if (i != 0) begin
                assign c_wires[i] = gen_wires[i-1] | prop_wires[i-1] & c_out[i-1];
            end
        end
    endgenerate
    assign pg = prop_wires[3] & prop_wires[2] & prop_wires[1] & prop_wires[0];
    assign gg = gen_wires[3] 
                | gen_wires[2] & prop_wires[3]          
                | gen_wires[1] & prop_wires[3] & prop_wires[2] 
                | gen_wires[0] & prop_wires[3] & prop_wires[2] & prop_wires[1];
    assign c64 = gen_wires[3] | prop_wires[3] & c_wires[3];

endmodule