import cocotb
from cocotb.types import Logic
from cocotb.triggers import Timer
import random
from random import randint

#inputs a, b, c0,
#outputs c4, sout, pg, gg 
def testBit(value, offset):
    mask = 1 << offset
    return 1 if (value & mask) != 0 else 0

async def run_inputs(dut, a, b, c0):
    dut.a.setimmediatevalue(a)
    dut.b.setimmediatevalue(b)
    dut.c0.setimmediatevalue(c0)
    await Timer(1, units="ns")
    assert dut.sout.value.integer == (a + b + c0) & 0xf #only 4 bits
    ab_or = 0
    for i in range(4):
        ab_or += 1 if ((testBit(a, i) == 1) or (testBit(b, i) == 1)) else 0
    pg_gold = 1 if ab_or == 4 else 0

    #could just do a | b == 15
    
    a_arr = [0, 0, 0, 0]
    b_arr = [0, 0, 0, 0]
    for i in range(4):
        a_arr[i] = testBit(a, i)
        b_arr[i] = testBit(b, i)

    gg_gold = (a_arr[3] & b_arr[3]) | (a_arr[2] & b_arr[2]) & (a_arr[3] | b_arr[3]) | (a_arr[1] & b_arr[1]) & (a_arr[3] | b_arr[3]) & (a_arr[2] | b_arr[2]) | (a_arr[0] & b_arr[0]) & (a_arr[3] | b_arr[3]) & (a_arr[2] | b_arr[2]) & (a_arr[1] | b_arr[1])
    
    assert dut.pg.value == pg_gold
    assert dut.gg.value == gg_gold
    assert dut.c4.value == gg_gold | pg_gold & c0


@cocotb.test()
async def all_zeroes(dut):
    a = 0
    b = 0
    cin = 0
    await run_inputs(dut, a, b, cin)

@cocotb.test()
async def all_ones(dut):
    a = 15
    b = 15
    cin = 0
    await run_inputs(dut, a, b, cin)

@cocotb.test()
async def num(dut):
    a = 10
    b = 5
    cin = 0
    await run_inputs(dut, a, b, cin)

@cocotb.test()
async def overflow_num(dut):
    a = 10
    b = 10
    cin = 0
    await run_inputs(dut, a, b, cin)

@cocotb.test()
async def random_tests(dut):
    """Try accessing the design."""
    #                     a  b  cin
    for i in range(15):
        a = randint(0, 15)
        b = randint(0, 15)
        cin = randint(0, 1)
        await run_inputs(dut, a, b, cin )
