module cla4 
(
    input  logic       c0, 
    input  logic [3:0] a, 
    input  logic [3:0] b,
    output logic       c4,
    output logic [3:0] sout,
    output logic       pg, 
    output logic       gg
);

    //generate 4 full_adders

    //assign
    logic c_wires    [3:0];
    logic prop_wires [3:0];
    logic gen_wires  [3:0];

    assign c_wires[0] = c0;

    genvar i;
    generate
        for (i = 0; i < 4; i++) begin

            full_adder_cp fa(a[i], b[i], c_wires[i], prop_wires[i], gen_wires[i], sout[i]);

            if (i != 0) begin
                assign c_wires[i] = gen_wires[i-1] | prop_wires[i-1] & c_wires[i-1];
            end
        end
    endgenerate
    assign pg = prop_wires[3] & prop_wires[2] & prop_wires[1] & prop_wires[0];
    assign gg = gen_wires[3] 
                | gen_wires[2] & prop_wires[3]          
                | gen_wires[1] & prop_wires[3] & prop_wires[2] 
                | gen_wires[0] & prop_wires[3] & prop_wires[2] & prop_wires[1];
    assign c4 = gen_wires[3] | prop_wires[3] & c_wires[3];

endmodule