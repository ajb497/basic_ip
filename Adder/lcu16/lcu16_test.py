import cocotb
from cocotb.types import Logic
from cocotb.triggers import Timer
import random
from random import randint

#inputs a, b, c0,
#outputs c16, sout, pg, gg 
def testBit(value, offset):
    mask = 1 << offset
    return 1 if (value & mask) != 0 else 0

def getBits(value, start, stop):
    return (value >> start) & (2**(stop-start+1) - 1)

def pg_calc( ):
    return (a | b) & 0xffff 

def gg_calc(a_arr, b_arr):
    return (a_arr[3] & b_arr[3]) | (a_arr[2] & b_arr[2]) & (a_arr[3] | b_arr[3]) | (a_arr[1] & b_arr[1]) & (a_arr[3] | b_arr[3]) & (a_arr[2] | b_arr[2]) | (a_arr[0] & b_arr[0]) & (a_arr[3] | b_arr[3]) & (a_arr[2] | b_arr[2]) & (a_arr[1] | b_arr[1])

async def run_inputs(dut, a, b, c0):
    dut.a.setimmediatevalue(a)
    dut.b.setimmediatevalue(b)
    dut.c0.setimmediatevalue(c0)
    await Timer(1, units="ns")
    assert dut.sout.value.integer == (a + b + c0) & 0xffff #only 4 bits
    
    ab_or = 0
    for i in range(16):
        ab_or += 1 if ((testBit(a, i) == 1) or (testBit(b, i) == 1)) else 0
    pg_gold = 1 if ab_or == 16 else 0

    #could just do a | b == 15
    
    gg_arr = [0, 0, 0, 0]
    pg_arr = [0, 0, 0, 0]
    a_arr2 = [0, 0, 0, 0]
    b_arr2 = [0, 0, 0, 0]
    pg_cnt = 0
    
    for i in range(4):
        pg_arr[i] = 1 if ((getBits(a, i*4, (i*4)+3) | getBits(b, i*4, (i*4)+3)) & 0xf == 0xf) else 0 
        pg_cnt += 1 if pg_arr[i] == 1 else 0
        for j in range(4):
            a_arr2[j] = testBit(a, (i*4)+j)
            b_arr2[j] = testBit(b, (i*4)+j)
        gg_arr[i] = gg_calc(a_arr2, b_arr2)

    gg_gold = gg_arr[3] | gg_arr[2] & pg_arr[3] | gg_arr[1] & pg_arr[3] & pg_arr[2] | gg_arr[0] & pg_arr[3] & pg_arr[2] & pg_arr[1]
    pg_gold = 1 if pg_cnt == 4 else 0
    
    assert dut.pg.value == pg_gold
    assert dut.gg.value == gg_gold
    assert dut.c16.value == gg_gold | pg_gold & c0


@cocotb.test()
async def all_zeroes(dut):
    a = 0
    b = 0
    cin = 0
    await run_inputs(dut, a, b, cin)

@cocotb.test()
async def all_ones(dut):
    a = 65535
    b = 65535
    cin = 0
    await run_inputs(dut, a, b, cin)

@cocotb.test()
async def num(dut):
    a = 10
    b = 5
    cin = 0
    await run_inputs(dut, a, b, cin)

@cocotb.test()
async def overflow_num(dut):
    a = 65535
    b = 10
    cin = 0
    await run_inputs(dut, a, b, cin)

@cocotb.test()
async def random_tests(dut):
    """Try accessing the design."""
    #                     a  b  cin
    for i in range(15):
        a = randint(0, 65535)
        b = randint(0, 65535)
        cin = randint(0, 1)
        await run_inputs(dut, a, b, cin )
