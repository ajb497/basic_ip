module register #(
    parameter Bits = 64
)
(
    input  logic            clk,
    input  logic            rstn_,
    input  logic [Bits-1:0] d,
    output logic [Bits-1:0] q
)

always_ff @( clk ) begin
    if (!rstn_) begin
       q <= 0; 
    end
    else if (en) begin
        q <= d;
    end
    else begin
        q <= q;
    end
end
endmodule