/*
    This is a multi-stage Kogge Stone Adder. This version was not designed for pipelining and only
    allows one calculation to begin every log2(N) cycles. It is not working because of the second 
    to last generate block.   
*/
module ks_add #(
    parameter N = 64
)

(
    input  logic         clk,
    input  logic         rstn_,
    input  logic         en,
    input  logic [N-1:0] a,
    input  logic [N-1:0] b,
    output logic [N-1:0] out
);

    //add local params for readability

    genvar i;
    generate
        for (i = 0; i < $clog2(N) + 1; i++) begin : gen_pg //wrong  64 63 62 60 56
            logic [(N-int'(2**(i-1)))-1:0] g_reg_array;
            logic [(N-int'(2**(i-1)))-1:0] p_reg_array;
            logic [(N-int'(2**(i-1)))-1:0] p_out_array;
            logic [(N-int'(2**(i-1)))-1:0] g_out_array;
        end
    endgenerate
    
    genvar j;
    generate
        for (j = 0; j < $clog2(N) + 1; j++) begin : reg_assign
            always_ff @( posedge clk ) begin
                if ( !rstn_ ) begin
                    gen_pg[j].g_reg_array <= 0;
                    gen_pg[j].p_reg_array <= 0;
                end
                else if ( en ) begin
                    gen_pg[j].g_reg_array <= gen_pg[j].g_out_array;
                    gen_pg[j].p_reg_array <= gen_pg[j].p_out_array;
                end
                else begin
                    gen_pg[j].g_reg_array <= gen_pg[j].g_reg_array;
                    gen_pg[j].p_reg_array <= gen_pg[j].p_reg_array;
                end
            end
        end
    endgenerate

    /*
        This part is wrong and will need to be redone to account for the shift in register addressing
        based on the step.
    */
    genvar step;
    generate 
        for (i = 0; i < N; i++) begin
            pg_0 step0(a[i], b[i], gen_pg[0].p_out_array[i], gen_pg[0].g_out_array[i]);
        end

        for (step = 1; step <= $clog2(N); step++) begin
            for (i = 0; i < N-2**(step-1); i++) begin //need to recalculate the N-steps
                if (i < 2**(step-2) && step > 1) begin
                    pg_1plus stepn(gen_pg[step-($clog2(i)+1)].p_reg_array[2**(step-1)],  //need to account for moving i
                             gen_pg[step-($clog2(i)+1)].g_reg_array[2**(step-1)], 
                             gen_pg[step-1].p_reg_array[i], //0 1
                             gen_pg[step-1].g_reg_array[i],
                             gen_pg[step].p_out_array[i], 
                             gen_pg[step].g_out_array[i]);
                end
                else begin
                    pg_1plus stepn(gen_pg[step-1].p_reg_array[2**(step-1)], //0 0 
                         gen_pg[step-1].g_reg_array[2**(step-1)], //0
                         gen_pg[step-1].p_reg_array[i], //0 1
                         gen_pg[step-1].g_reg_array[i],
                         gen_pg[step].p_out_array[i], 
                         gen_pg[step].g_out_array[i]);
                end
            end
        end
    endgenerate

    generate
        for (i = 0; i < N; i++) begin
            if ( i==0 ) begin
                assign out[0] = gen_pg[0].p_reg_array[0];
            end 
            else if ( i==1 ) begin
                assign out[1] = gen_pg[0].p_reg_array[1] ^ gen_pg[0].g_reg_array[0];
            end
            /*
            if ( i==N ) begin //overflow bit 64
                assign out[N-1] = gen_pg[$clog2(i-1)].g_reg_array[N-1]; //need to remember how it works
            end
            */
            else begin //2 through N-1
                assign out[i] = gen_pg[0].p_reg_array[i] ^ gen_pg[$clog2(i-1)].g_reg_array[i-1]; 
            end
        end
    endgenerate

endmodule