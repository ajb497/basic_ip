/*
    This is a multi-stage Kogge Stone Adder. This version was not designed for pipelining and only
    allows one calculation to begin every log2(N) cycles. It is not working because of the last 
    generate block.   
*/
module ks_add_pipe #(
    parameter N = 64
)

(
    input  logic         clk,
    input  logic         rstn_,
    input  logic         en,
    input  logic [N-1:0] a,
    input  logic [N-1:0] b,
    output logic [N-1:0] out
);

    localparam stages = $clog2(N) + 1;

    genvar i;
    generate
        for (i = 0; i < stages; i++) begin : gen_pg
            logic [N-1:0] g_reg_array;
            logic [N-1:0] p_reg_array;
            logic [N-1:0] p_out_array;
            logic [N-1:0] g_out_array;
        end
    endgenerate
    
    genvar j;
    generate
        for (j = 0; j < stages; j++) begin : reg_assign
            always_ff @( posedge clk ) begin
                if ( !rstn_ ) begin
                    gen_pg[j].g_reg_array <= 0;
                    gen_pg[j].p_reg_array <= 0;
                end
                else if ( en ) begin
                    gen_pg[j].g_reg_array <= gen_pg[j].g_out_array;
                    gen_pg[j].p_reg_array <= gen_pg[j].p_out_array;
                end
                else begin
                    gen_pg[j].g_reg_array <= gen_pg[j].g_reg_array;
                    gen_pg[j].p_reg_array <= gen_pg[j].p_reg_array;
                end
            end
        end
    endgenerate

    genvar step;
    generate 
        for (i = 0; i < N; i++) begin
            pg_0 step0(a[i], b[i], gen_pg[0].p_out_array[i], gen_pg[0].g_out_array[i]);
        end

        for (step = 1; step <= stages-1; step++) begin
            for (i = 0; i < N; i++) begin
                if (i >= 2**(step-1)) begin //need to recalculate the N-steps
                    pg_1plus stepn(gen_pg[step-1].p_reg_array[i], //0 0 
                                   gen_pg[step-1].g_reg_array[i], //0
                                   gen_pg[step-1].p_reg_array[i-2**(step-1)], //0 1
                                   gen_pg[step-1].g_reg_array[i-2**(step-1)],
                                   gen_pg[step].p_out_array[i], 
                                   gen_pg[step].g_out_array[i]);
                end
                else begin
                    assign gen_pg[step].p_out_array[i] = gen_pg[step-1].p_reg_array[i];
                    assign gen_pg[step].g_out_array[i] = gen_pg[step-1].g_reg_array[i];
                end
            end
        end
    endgenerate

    generate
        for (i = 0; i < N; i++) begin
            if ( i==0 ) begin
                assign out[0] = gen_pg[stages-1].p_reg_array[0]; 
            end 
            /*
            else if ( i==N ) begin //overflow bit 64
                assign overflow = gen_pg[stages-1].g_reg_array[N-1]; //need to remember how it works
            end
            */
            else begin //2 through N-1
                assign out[i] = gen_pg[stages-1].p_reg_array[i] ^ gen_pg[stages-1].g_reg_array[i-1]; 
            end
        end
    endgenerate

endmodule