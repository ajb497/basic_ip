import cocotb
from cocotb.types import Logic
from cocotb.triggers import Timer
import random
from random import randint

async def run_inputs(dut, a, b):
    dut.rstn_.setimmediatevalue(0)
    await Timer(2, units="ns")
    dut.rstn_.setimmediatevalue(1)
    dut.a.setimmediatevalue(a)
    dut.b.setimmediatevalue(b)
    dut.en.setimmediatevalue(1)
    await Timer(18, units="ns")
    assert dut.out.value.integer == (a + b) & 0xffffffffffffffff #only 4 bits

async def generate_clock(dut):
    """Generate clock pulses."""

    for cycle in range(10):
        dut.clk.value = 0
        await Timer(1, units="ns")
        dut.clk.value = 1
        await Timer(1, units="ns")

@cocotb.test()
async def all_zeroes(dut):
    a = 0
    b = 0
    await cocotb.start(generate_clock(dut))
    await run_inputs(dut, a, b)


@cocotb.test()
async def three(dut):
    a = 1
    b = 2
    await cocotb.start(generate_clock(dut))
    await run_inputs(dut, a, b)

"""
@cocotb.test()
async def num(dut):
    a = 10
    b = 5
    cin = 0
    await run_inputs(dut, a, b, cin)

@cocotb.test()
async def overflow_num(dut):
    a = 65535
    b = 10
    cin = 0
    await run_inputs(dut, a, b, cin)

"""