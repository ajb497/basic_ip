module full_adder_cp  
(
    input  logic a, 
    input  logic b, 
    input  logic cin,
    output logic prop,
    output logic gen, 
    output logic sout
);

assign sout = a ^ b ^ cin;
assign prop = a | b;
assign gen = a & b; 

endmodule